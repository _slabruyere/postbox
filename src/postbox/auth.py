"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


import requests
import json
import logging
import os
import grpc
from aiohttp import web
from jose import jwt
from jose.exceptions import JWTError
from postbox import write_network_scope, read_network_scope, write_metrics_scope
from postbox.config import Config

# TODO: These need to go in a config file
ALGORITHMS = ["RS256"]
API_AUDIENCE = "https://evolve-ingestor/"
AUTH0_DOMAIN = "zepben.au.auth0.com"
AUTH_CONFIG = {"alg": ALGORITHMS, "aud": API_AUDIENCE, "dom": f"https://{AUTH0_DOMAIN}/oauth/token"}

_AUTH_HEADER_KEY = 'authorization'
# TODO this needs error checking, caching
# When scopes change access token will also need to be re-fetched. but i guess this is a user managed thing?
# but probably need to provide some refresh functionality... maybe just a 30 minute cache?
log = logging.getLogger(__name__)

required_scopes = {
    '/network.NetworkGraph/AddChunk': write_network_scope,
    '/network.NetworkGraph/GetNetwork': read_network_scope,
    '/network.NetworkData/RecordActivePower': write_metrics_scope,
    '/pb.MeterReadings/sendMeterReading': write_metrics_scope,
    '/zepben.pb.MeterReadings/createVoltageReading': write_metrics_scope,
    '/zepben.pb.MeterReadings/createRealPowerReading': write_metrics_scope,
    '/zepben.pb.MeterReadings/createReactivePowerReading': write_metrics_scope,
    '/zepben.pb.NetworkData/createBaseVoltage': write_network_scope,
    '/zepben.pb.NetworkData/getWholeNetwork': read_network_scope,
}


async def auth_config_server(_):
    return web.Response(text=json.dumps(AUTH_CONFIG))


def start_conf_server():
    app = web.Application()
    app.add_routes([web.get('/auth_config', auth_config_server)])
    web.run_app(app)


# TODO: make this handle arbitrary filepaths - currently only handles relative paths from this directory.
def load_credential_from_file(filepath):
    real_path = os.path.join(os.path.dirname(__file__), filepath)
    with open(real_path, 'rb') as f:
        return f.read()


class AuthError(Exception):
    def __init__(self, error, description, status_code):
        super().__init__(description)
        self.error = error
        self.status_code = status_code


# Format error response and append status code
class AuthTokenInterceptor(grpc.ServerInterceptor):

    def __init__(self):
        self.config = Config()
        self.jwks = None
        self.refresh_jwks()

    @staticmethod
    def abort_with_error(error):
        def abort(_, context):
            context.abort(grpc.StatusCode.UNAUTHENTICATED, f"{error.error} - {error}")
        return grpc.unary_unary_rpc_method_handler(abort)

    def intercept_service(self, continuation, handler_call_details):
        # Example HandlerCallDetails object:
        #     _HandlerCallDetails(
        #       method=u'/helloworld.Greeter/SayHello',
        #       invocation_metadata=...)
        log.debug(handler_call_details)

        try:
            token = None
            for metadata in handler_call_details.invocation_metadata:
                if metadata.key.lower() == _AUTH_HEADER_KEY:
                    token = self.get_token(metadata.value)
                    break  # we only support a single auth header, so break here even if it's not valid
            else:
                self.abort_with_error(AuthError("missing_auth_header", "Authorization header was not provided", 400))

            self.authorise(token, required_scopes[handler_call_details.method])
            return continuation(handler_call_details)
        except AuthError as ae:
            # This will send the error back to the client
            return self.abort_with_error(ae)

    @staticmethod
    def get_token(bearer_token):
        """
        Checks if provided token conforms to OAuth2 bearer token standards.
        :return: The token portion of the bearer token if it passes validation
        """
        if not bearer_token:
            raise AuthError("missing_bearer_token", "Auth header was missing a token", 401)

        try:
            parts = bearer_token.split()
            if parts[0].lower() != "bearer":
                raise AuthError("invalid_bearer_token", "Bearer token didn't conform to standards. Missing: Bearer", 401)
            elif len(parts) == 1:
                raise AuthError("invalid_bearer_token", "Bearer token didn't conform to standards. Missing token value", 401)
            elif len(parts) > 2:
                raise AuthError("invalid_bearer_token", "Bearer token invalid. Should be 'Bearer: <token>'", 401)
        except Exception as e:
            raise AuthError("invalid_token", e, 400)
        bearer_token = parts[1]
        return bearer_token

    def refresh_jwks(self):
        self.jwks = json.loads(requests.get(self.config.jwks).text)

    def _get_key_from_jwks(self, key_id):
        """
        Find key in JWKs. If it's missing, will refresh the jwks and check again.
        :returns: dict representing the RSA key, with at least the following fields: kty, kid, use, n, e, x5c
        :raises AuthError: When no key is found
        """
        for key in self.jwks["keys"]:
            if key["kid"] == key_id:
                return key
        else:
            self.refresh_jwks()
            for key in self.jwks["keys"]:
                if key["kid"] == key_id:
                    return key
            raise AuthError("invalid_key", f"Unable to find appropriate key in jwks at {jwks}", 400)

    def authenticate(self, token):
        """
        Determines if the Access Token is valid by verifying signing of the contents of the JWT
        :param token: The JWT to verify
        :raises AuthError: with the following potential error codes:
            400 invalid_header - if the authorization header is incorrect and not a Bearer token
            400 invalid_jwt_header - If decoding of the header fails
            400 invalid_claims - If claims were invalid.
            400 invalid_key - If the signing key couldn't be found in the issuers keyset
            401 token_expired - If token is past its expiry
        """
        try:
            unverified_header = jwt.get_unverified_header(token)
        except JWTError as je:
            raise AuthError("invalid_jwt_header", f"{je} Make sure your token is correct!", 400)

        rsa_key = self._get_key_from_jwks(unverified_header['kid'])
        try:
            return jwt.decode(
                token,
                rsa_key,
                # TODO: move to config, set as fields of the class
                algorithms=ALGORITHMS,
                audience=API_AUDIENCE,
                issuer="https://"+AUTH0_DOMAIN+"/"
            )
        except jwt.ExpiredSignatureError:
            raise AuthError("token_expired", "Token is expired", 401)
        except jwt.JWTClaimsError:
            raise AuthError("invalid_claims", "Incorrect claims specified - please check the audience and issuer", 400)
        except Exception as e:
            log.debug(f'Failed to decode token with request: {e}')
            raise AuthError("invalid_header", f"Unable to parse authentication token. error was: {e}", 401)

    def authorise(self, token, required_scope, verify=True):
        """
        Determines if the required scope is present in the Access Token.
        :param token: VERIFIED JWT containing scopes
        :param required_scope: The scope required to access the resource
        :param verify: If true we will verify the JWT using :func:`authenticate`
        :raises AuthError: If verify fails
        :return: True if required scope was present in the tokens scopes
        """
        if verify:
            claims = self.authenticate(token)
            log.debug(f"Verified token {token}")
        else:
            claims = jwt.get_unverified_claims(token)
        log.debug(f"Token has claims: {claims}")
        token_scopes = claims["scope"].split()
        for token_scope in token_scopes:
            if token_scope == required_scope:
                log.debug(f"Token contained required claim: {required_scope}")
                return True

        raise AuthError("missing_claim", f"Token did not have the required claim {required_scope}", 403)
