"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


import zepben.postbox.pb_pb2_grpc as pb_grpc
from grpc import StatusCode
from zepben.model.metrics_store import MetricsStore
from zepben.model.network import Network
from zepben.model.metering import ReadingType, MeterReading
from zepben.model.exceptions import ReadingException
from zepben.postbox.pb_pb2 import MRResponse


class MeterReadingsServicer(pb_grpc.MeterReadingsServicer):

    def __init__(self, metrics_store: MetricsStore, network: Network):
        self.metrics_store = metrics_store
        self.network = network

    """
    TODO: We currently don't store the meters anywhere, we just extract the readings and use those only.
    This will need reworking in the future, but we will leave it until we implement a database layer.
    """

    def createVoltageReading(self, request, context):
        try:
            mr = MeterReading.from_pb(request.meterReading, self.network, reading_type=ReadingType.VOLTAGE)
            self.metrics_store.store_meter_reading(mr, reading_type=ReadingType.VOLTAGE)
        except ReadingException as r:
            context.set_details(str(r))
            context.set_code(StatusCode.UNKNOWN)
            return MRResponse()

        return MRResponse()

    def createRealPowerReading(self, request, context):
        try:
            mr = MeterReading.from_pb(request.meterReading, self.network, reading_type=ReadingType.REAL_POWER)
            self.metrics_store.store_meter_reading(mr, reading_type=ReadingType.REAL_POWER)
        except ReadingException as r:
            context.set_details(str(r))
            context.set_code(StatusCode.UNKNOWN)
            return MRResponse()
        return MRResponse()

    def createReactivePowerReading(self, request, context):
        try:
            mr = MeterReading.from_pb(request.meterReading, self.network, reading_type=ReadingType.REACTIVE_POWER)
            self.metrics_store.store_meter_reading(mr, reading_type=ReadingType.REACTIVE_POWER)
        except ReadingException as r:
            context.set_details(str(r))
            context.set_code(StatusCode.UNKNOWN)
            return MRResponse()
        return MRResponse()

    def getVoltageReadings(self, request, context):
        for meter_reading in self.metrics_store.get_readings(ReadingType.VOLTAGE):
            pb_mr = meter_reading.to_pb()
            yield pb_mr

    def getReactivePowerReadings(self, request, context):
        for meter_reading in self.metrics_store.get_readings(ReadingType.REACTIVE_POWER):
            pb_mr = meter_reading.to_pb()
            yield pb_mr

    def getRealPowerReadings(self, request, context):
        for meter_reading in self.metrics_store.get_readings(ReadingType.REAL_POWER):
            pb_mr = meter_reading.to_pb()
            yield pb_mr
