"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


from zepben.postbox.pb_pb2 import Identity
from .conftest import FakeContext


class TestNetworkServicer(object):

    def test_send(self, network):
        assert len(network.energy_sources) > 0

    def test_get_equipment(self, network):
        ns, _ = network
        stream = ns.getEquipmentContainer(Identity(), FakeContext())
        equip_iter = iter(stream)
        # TODO: more asserts
        es = next(equip_iter).es
        assert es.mRID == "1"
        l1 = next(equip_iter).acls
        assert l1.mRID == "2"
        pt1 = next(equip_iter).pt
        assert pt1.mRID == "3"
        l2 = next(equip_iter).acls
        assert l2.mRID == "4"
        ec1 = next(equip_iter).ec
        assert ec1.mRID == "5"
